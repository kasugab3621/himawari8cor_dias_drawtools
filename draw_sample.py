#!/usr/bin/env python3
import xarray as xr
import matplotlib.pyplot as plt

da = xr.open_dataset("./sample_dias/HS_H08_20180204_2050_B13_JP01_R20.nc")
print(da)

da.tbb.plot(cmap="Greys")
plt.savefig("sample.png")

